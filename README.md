# MYA

MYA (Manage Your Api) is an Open Source solution using PHP providing a set of methods allowing API Management functionalities.<br/>The class can be easily integrated into WS projects written in PHP.

MYA Package

OpenSource license

<strong>How to install:</strong><br>
MYA is available on Packagist. To install, use Composer: ```composer require cfd-innovationoss/mya-public ```.<br>
Or you can also install it with a git clone https://gitlab.com/cfd-innovationoss/mya-public.git in your root project.

<strong>How to use:</strong><br>
Add the following lines in your webservice:

```php
include 'mya.php';
$myRequest = new mya;
$myRequest->collect("csv");
```

The collect() method accepts 2 parameters, the log format (json or csv) et an optional name for the log file.
Exemple: collect("json", "new_api")
