<?php
/**
 * Class mya
 * Create log files in mya folder with all the main information about the webservice request
 *
 */
class mya
{
    private $method;
    private $clientADDR;
    private $date;
    private $headerString ;
    private $auth;

    private $body ;
    private $nomDuScript;

    public function __construct(){

        //Retrieval of the WS method
        $this->method = $_SERVER['REQUEST_METHOD'];

        //Retrieval of the Remote IP
        $this->clientADDR = $_SERVER['REMOTE_ADDR'];;
        $this->date = date("d-m-Y H:i:s");

        //Retrieval authentication username
        $this->auth = $_SERVER['PHP_AUTH_USER'];;


        //Retrieval of the header content in json format
        $this->headerString = json_encode(apache_request_headers());

        //Retrieval of the body content if the method is not "GET"
        if($this->method != "GET"){
            $this->body = file_get_contents('php://input');
            $this->body = str_replace("\n","",$this->body);
            $this->body = str_replace("\r","",$this->body);
            $this->body = str_replace("\t","",$this->body);
        }
        else{
            $this->body = "\"\"";
        }

        //Retrieval of the name of the WS
        $uri = explode('/',$_SERVER['REQUEST_URI']) ;
        $this->nomDuScript = end($uri) ;
    }


    /**
     * Main function with files creation
     *
     * @param string format: "csv" or "json" |"csv" by default
     * @param string nomLog: additional string for file name | empty by default
     * @return bool 1 if ok, 0 if trouble while file filling
     */
    function collect($format='csv', $nomLog=''){

        if ($nomLog != '') $nomLog = '_'.$nomLog;

        if (!is_dir("mya")) {
            //the mya log folder does not exist, we must create it:
            mkdir('mya');
        }

        if($format == 'csv'){
            //File naming with the information
            $file_path = 'mya/'.$this -> nomDuScript.$nomLog.'.csv';
            try {
                //Inclusion of the request information in csv format,
                //";" is the .csv delimiter
                file_put_contents($file_path,
                    $this -> date.";".
                    $this -> clientADDR.";".
                    $this -> auth.";".
                    $this -> method.";".
                    $this -> headerString.";".
                    $this -> body."\n",
                    FILE_APPEND);
                return 1;
            } catch (Exception $e) {
                return 0;
            }
        }
        elseif($format == 'json'){
            //File naming with the information
            $file_path = 'mya/'.$this -> nomDuScript.$nomLog.'.json';

            if (!file_exists($file_path)) {
                //File creation and insertion of the first square bracket
                file_put_contents($file_path,"[\n");
            }
            else{
                //If the file already exists, we remove the closing square bracket to add a new request to the log file.
                file_put_contents($file_path, str_replace("\n]", ",", file_get_contents($file_path)));
            }

            try {
                //Inclusion of the request information in json format,
                file_put_contents($file_path,
                    "\n{\n\t\"date\": \"".$this -> date."\",\n".
                    "\t\"remote_ip\": \"".$this -> clientADDR."\",\n".
                    "\t\"auth-Username\": \"".$this -> auth."\",\n".
                    "\t\"method\": \"".$this -> method."\",\n".
                    "\t\"header\": ".$this -> headerString.",\n".
                    "\t\"body\": ".$this -> body."\n}\n]",
                    FILE_APPEND);
                return 1;
            } catch (Exception $e) {
                return 0;
            }
        }
        return 0;
        //...
    }
}